const express =  require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const requireDir = require("require-dir");

//Starting App
const app = express();
app.use(express.json());
app.use(cors());

//Starting DB
/*mongoose.connect(
    "mongodb://localhost:27017/nodeapi", 
    {useNewUrlParser: true, useUnifiedTopology: true }
);*/

//mongo atlas
mongoose.connect('mongodb+srv://pitaya:pitaya@cluster0-zgvlo.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true, useUnifiedTopology: true
});

requireDir("./src/models");

//Routes
app.use('/api', require('./src/routes'));

app.listen("3001");