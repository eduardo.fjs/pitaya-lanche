const mongoose = require("mongoose");
const mongoosePaginate =  require("mongoose-paginate");
const Item = require("../models/Item");

const OrderSchema = new mongoose.Schema({
    itens:[{
        type: String,
        ref: "Item",
    }],
    subtotal:{
        type: Number,
        required: true,
    },
    status:{ 
        type: String,
        required: true,
    },
    createdAt:{
        type: Date,
        default: Date.now,
    },
});

OrderSchema.plugin(mongoosePaginate);

mongoose.model("Order", OrderSchema);