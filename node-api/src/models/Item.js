const mongoose = require("mongoose");
const mongoosePaginate =  require("mongoose-paginate");

const ItemSchema = new mongoose.Schema({
    quantity:{
        type: Number,
        required: true,
    },
    total:{ 
        type: Number,
        required: true,
    },
     createdAt:{
        type: Date,
        default: Date.now,
    },
});

ItemSchema.plugin(mongoosePaginate);

mongoose.model("Item", ItemSchema);