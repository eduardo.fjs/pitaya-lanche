const express = require('express');
const routes = express.Router();

//Item

const ItemController = require('./controllers/ItemController');

routes.get("/items", ItemController.index); //Listar
routes.get("/items/:id", ItemController.show); // visualizar
routes.post("/items", ItemController.store); // criar (adicionar ao carrinho)
routes.put("/items/:id", ItemController.update); //
routes.delete("/items/:id", ItemController.destroy); //

//Order

const OrderController = require('./controllers/OrderController');

routes.get("/orders", OrderController.index);
routes.get("/orders/:id", OrderController.show);
routes.post("/orders", OrderController.store);
routes.put("/orders/:id", OrderController.update);
routes.delete("/orders/:id", OrderController.destroy);

//Product
const ProductController = require('./controllers/ProductController');

routes.get("/products", ProductController.index);
routes.get("/products/:id", ProductController.show);
routes.post("/products", ProductController.store);
routes.put("/products/:id", ProductController.update);
routes.delete("/products/:id", ProductController.destroy);

//User

const UserController = require('./controllers/UserController');

routes.get("/users", UserController.index);
routes.get("/users/:id", UserController.show);
routes.post("/users", UserController.store);
routes.put("/users/:id", UserController.update);
routes.delete("/users/:id", UserController.destroy);

module.exports = routes