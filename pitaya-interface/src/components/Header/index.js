import React from "react";
import logo from "./logo.png";
import carrinho from "./shopping-cart.png";

import "./styles.css";

const Header = () => (
    <header id="main-header">
        <img src={logo} height="200" alt="Pitaya Logo"/>
        <div class="absolute">
            <img src={carrinho} height="30" alt="Icone carrinho"/>

        </div>
    </header>
   
    
    // <header id="main-header">Pitaya Lanche</header>
);

export default Header;