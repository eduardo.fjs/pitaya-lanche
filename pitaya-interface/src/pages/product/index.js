import React, {Component} from "react";
import api from "../../services/api";
import ok from "./check.png";

import "./styles.css"

export default class Product extends Component{
    state = {
        product: {},
    };

    async componentDidMount(){
        const { id } = this.props.match.params;

        const response = await api.get(`/products/${id}`);

        this.setState({ product: response.data });
    }

    render() {
        const { product } = this.state;

        return(
            <div className="product-info">
                <h1>Produto adicionado ao carrinho</h1>
                <p>{product.description}</p>

                <p>
                    Preço: R$ <a>{product.price}</a>
                </p>
                {/* <p>
                    URL: <a href={product.url}>{product.url}</a>
                </p> */}
                <img src={ok} height="30" alt="Icone confirma"/>
            </div>  
        );
    }
}