import React, {Component} from "react";

import Routes from "./routes"; 
import api from "./services/api";


import "./styles.css";

import Header from "./components/Header";
import Main from './pages/main';
// import Start from "./pages/start


const App = () => (
  <div className="App">
    <Header/>
    <Routes/>

    <Main/>
  </div>

);

export default App;
